#' Load the latest version of a file in a directory
#'
#' This function leads to the latest version from a file in a directory, which contains multiple versions of the file. The file names differ onyl by the time and date they were downloaded.
#'
#' @param inputdata needs spec
#'
#' @return This function prints data
#'
#' @export

# define function to print the data
print_data = function(inputdata, sdb_file){
  if("NES" %in% names(inputdata) && "ID" %in% names(inputdata) && "setSize" %in% names(inputdata) && "p.adjust" %in% names(inputdata)){
  inputdata_sig = inputdata # [inputdata$p.adjust <= 0.05, c("ID", "setSize", "enrichmentScore", "NES", "p.adjust")]
  inputdata_sig$temp_label = inputdata_sig$ID
  inputdata_sig = inputdata_sig %>%
    left_join(sdb_file, by = c("temp_label")) %>%
    relocate(term, .before = everything()) %>%
    mutate(temp_label = NULL) %>%
    mutate(category = NULL) %>%
    mutate(term = NULL) #%>%
    #mutate(description = NULL)
  return(inputdata_sig)
  } else {
    print(paste("only the following columns are in the data: ", names(inputdata), sep=" "))
  }
}


# old function
# define a function, which collates the data to show on the html file
# print_data = function(inputdata){
#   if (!is.null(inputdata) && !is.null(inputdata@result) && nrow(inputdata@result) > 0) {
#     inputdata_sig = inputdata@result[inputdata@result$p.adjust <= 0.05, c("setSize", "enrichmentScore", "NES", "p.adjust")]
#     inputdata_sig$temp_label = row.names(inputdata_sig)
#     inputdata_sig = inputdata_sig %>%
#       left_join(sdb_data, by = c("temp_label")) %>%
#       relocate(term, .before = everything()) %>%
#       mutate(temp_label = NULL) %>%
#       mutate(category = NULL)
#     return(inputdata_sig)
#   } else {
#     cat(print(paste0(deparse(substitute(inputdata)), " - No valid data for printing.")))
#     return(NULL)
#   }
# }
