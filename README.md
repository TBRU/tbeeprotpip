---
output:
  html_document: default
  pdf_document: default
---
# tbeeprotpip

## Description
'tbeeprotpip' is an R-based proteome analysis pipeline for _Mycobacterium tuberculosis_, mainly working with [protti](https://jpquast.github.io/protti/). 

### How to run the pipeline
Run the pipeline as follows. Example for comparison of WT to S450L in L2.
```
sbatch --job-name JOB_NAME SLURM_SCRIPT.sh /sciCore/MAIN/PATH/TO/PROJECT OUTPUTFILE_NAME EXPERIMENT_NAME ANNOTATION_FILE.csv

e.g.:
sbatch --job-name tbeeprotpip_wt_vs_s450l_L2 tbeeprotpip_slurm_script.sh /scicore/home/gagneux/bousel00/tb/TbX015 wt_vs_s450l_L2_output TbX015 annotation_file_wt_vs_s450l_L2.csv
```

For comparisons comprising more than two N_NUMBERS (e.g. 'WT vs S450L' with pooled lineages), use the slurm script which reserves more RAM: `tbeeprotpip_slurm_script_bigRAM.sh`

| slurm script                       | RAM reserved | Time reserved |
| :--------------------------------: | :----------: | :-----------: |
| tbeeprotpip_slurm_script.sh        | 128GB        | 30min         |
| tbeeprotpip_slurm_script_bigRAM.sh | 320GB        | 6h            |  

### Requirements
have spectronaut viewer
have scicore account


### Overview
0. To do before working running the pipeline  
    0.1 Download the Spectronaut file 
    0.2 Export data from Spectronaut
    0.3 Generate comparison annotation files (separate script)
    0.4 Make sure all R-packages are installed and working
1. Input preparation  
    1.1 Loading all data files  
    1.2 Annotate data  
    1.3 Filter data  
2. QC peptide level  
3. QC protein level  
4. Differential expression analysis  
5. Enrichment Analysis  
    5.1 Gene Ontology  
        5.1.1 Molecular Function  
        5.1.2 Cellular Component  
        5.1.3 Biological Process  
    5.2 KEGG Pathways  
        5.2.1 _Mtb_ H37Rv specific pathways  
        5.2.2 general(?) pathways  
    5.3 Mycobrowser  
        5.3.1 Functional Category  
        5.3.2 Function  
        5.3.3 Product  
    5.4 Further Annotations  
        5.4.1 UniProt Annotated Keywords  
        5.4.2 StringDB Local Network cluster  
        5.4.3 StringDB Subcellular Localization (compartments)  
        5.4.4 StringDB Reactome Pathways  
6. Summarizing all the data  
7. Separate scripts  
    7.1 All Sample correlation heat-map  
    7.2 Calculate euclidean distance between samples  
    7.3 Heatmaps of custom gene sets  
    7.4 Venn Diagrams of DE proteins  
    7.5 Plots of extent of DE between the three lineages  
    

***

## 0. To do before working running the pipeline  
###     0.1 Download the Spectronaut file 
###     0.2 Export data from Spectronaut
###     0.3 Generate comparison annotation files (separate script)
###     0.4 Make sure all R-packages are installed and working
## 1. Input preparation  
###     1.1 Loading all data files  
###     1.2 Annotate data  
###     1.3 Filter data  
## 2. QC peptide level  
## 3. QC protein level  
## 4. Differential expression analysis  
## 5. Enrichment Analysis  
###     5.1 Gene Ontology  
#####         5.1.1 Molecular Function  
#####         5.1.2 Cellular Component  
#####         5.1.3 Biological Process  
###     5.2 KEGG Pathways  
#####         5.2.1 _Mtb_ H37Rv specific pathways  
#####         5.2.2 general(?) pathways  
###     5.3 Mycobrowser  
#####         5.3.1 Functional Category  
#####         5.3.2 Function  
#####         5.3.3 Product  
###     5.4 Further Annotations  
#####         5.4.1 UniProt Annotated Keywords  
#####         5.4.2 StringDB Local Network cluster  
#####         5.4.3 StringDB Subcellular Localization (compartments)  
#####         5.4.4 StringDB Reactome Pathways  
## 6. Summarizing all the data  
## 7. Separate scripts  
###     7.1 All Sample correlation heat-map  
###     7.2 Calculate euclidean distance between samples  
###     7.3 Heatmaps of custom gene sets  
###     7.4 Venn Diagrams of DE proteins  
###     7.5 Plots of extent of DE between the three lineages 

## 1. PipelineTB

### 1.1 Bioinformatics softwares included in the pipeline

##### 1.1.1 Input data validation and quality control

Since one of the main sources of genomes is the public domain, fastQ files downloaded vary in format and quality. Thus the first main step of the pipeline is to validate the format of the fastQ files using **[fastQValidator](https://genome.sph.umich.edu/wiki/FastQValidator)**.

Specifically, **[fastQValidator](https://genome.sph.umich.edu/wiki/FastQValidator)** scans the fastQ and returns a value of overall file compliance.

In addition, a minimum value of read-length is given. If the fastQ complies with the minimum read-length set (can be fine-tuned using the argument `--read-length`), and with the fastQ format requirements, the value returned is `FASTQ_SUCCESS`. Otherwise, values such as `FASTQ_INVALID` or `FASTQ_READ_ERROR` are returned to indicate problems related to the format of the reads or the file, and the pipeline is stopped at this early stage.

We next use **FastQC** to generate basic statistics on the reads: read length (L) and number of reads (N), from which we infer an approximation of the coverage (LN/ size of genome). We then use the **SeqIO package of BioPython** to detect the phred encoding.




 
     0.1 Download the Spectronaut file (.sne) from the folder where the [PCF](https://www.biozentrum.unibas.ch/research/research-groups/research-groups-a-z/overview/unit/proteomics-core-facility) shares the output files.
    0.2 Load the Spectronaut file into Spectronaut and export the data set with all needed columns for protti `MSstats_BS_add_for_protti`. Loading the Export Schema file /PATH/in/tbeeprotpip/to/FILE will automatically let you export the desired columns.
 
  
Write to not download sne file with WinSCP but ood. 
write how to name the stringdb file name


When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.



## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

You need to have the following R packages installed: `protti`,`magrittr`,`dplyr`,`tidyverse`,`ggplot2`,`gplots`,`ggpubr`,`VennDiagram`,`ggvenn`,`KEGGREST`,`Peptides`,`stringr`,`fgsea`,`ggnewscale`,`clusterProfiler`,`pathview`,`enrichplot`,`biomaRt`,`ggrepel`,`msigdbr`,`ggridges`,`lattice`.

All these packages can be installed using the function `install.packages`:

```
install.packages("protti")
install.packages("magrittr")
install.packages("dplyr")
# packages needed for data-manipulation & depiction
install.packages("tidyverse")            
install.packages("ggplot2")            
install.packages("gplots")         # for the textplot function   
install.packages("ggpubr")         # for adding r and p values to plots with regression line   
install.packages("VennDiagram")            
install.packages("ggvenn")          # as a ggplot2 extension) alternative for VennDiagram   
# packages for easy calculation of peptide masses) charges etc.
install.packages("KEGGREST")       # load KEGGREST to access all aa seqs
install.packages("Peptides")       #install.packages("Peptides") dependencies=TRUE)  # install package to calculate aa seq weight etc     
# packages for enrichment analysis
install.packages("stringr")         # for generating the gmt file in enrichment analysis
install.packages("fgsea")           # for gene set enrichment analysis
install.packages("ggnewscale")      # for enrichment analysis
install.packages("clusterProfiler") # for enrichment analysis          
install.packages("pathview")        # for enrichment analysis    
install.packages("enrichplot")      # for enrichment analysis      
install.packages("biomaRt")         # for enrichment analysis  
install.packages("ggrepel")         # for enrichment analysis  
install.packages("msigdbr")         # for enrichment analysis  
install.packages("ggridges")        # for enrichment analysis 
install.packages("lattice")
```


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html) or push an existing Git repository with the following command:

***
