#!/bin/bash

#SBATCH --job-name=compress_raw_files                   #This is the name of your job
#SBATCH --cpus-per-task=1                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=1G              #This is the memory reserved per core.
#Total memory reserved: 1GB

#SBATCH --time=06:00:00        #This is the time that your task will run
#SBATCH --qos=6hours           #You will run in this queue

# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=/scicore/home/gagneux/GROUP/proteomics/PROJECTNAME/PCF_data/compress_raw_files.oe     #This is the joined STDOUT and STDERR file      


#This job runs from the current working directory
cd /scicore/home/gagneux/GROUP/proteomics/TbX016/PCF_data

#Remember:
#The variable $TMPDIR points to the local hard disks in the computing nodes.
#The variable $HOME points to your home directory.
#The variable $SLURM_JOBID stores the ID number of your job.


#load your required modules below
#################################


#export your required environment variables below
#################################################


#add your command lines below
#############################
gzip -r all_raw_files_compressed
