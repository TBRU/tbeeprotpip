#!/bin/bash

#SBATCH --job-name=tbeeprotpip_v2
#SBATCH --cpus-per-task=1                  #This is the number of cores reserved
#SBATCH --mem-per-cpu=128G              #This is the memory reserved per core.

#SBATCH --time=00:30:00        #This is the time that your task will run
#SBATCH --qos=30min           #You will run in this queue


# Paths to STDOUT or STDERR files should be absolute or relative to current working directory
#SBATCH --output=/scicore/home/gagneux/GROUP/proteomics/TbX016/pipeline_run_OUTERR_files/%x.%j.o     #These are the STDOUT and STDERR files
#SBATCH --error=/scicore/home/gagneux/GROUP/proteomics/TbX016/pipeline_run_OUTERR_files/%x.%j.e

#This job runs from the current working directory
# cd /scicore/home/gagneux/GROUP/proteomics/tbeeprotpip_Rpackage/inst/pipeline_runfile


#Remember:
#The variable $TMPDIR points to the local hard disks in the computing nodes.
#The variable $HOME points to your home directory.
#The variable $SLURM_JOBID stores the ID number of your job.


#load your required modules below
#################################
## module load R/4.2.2-foss-2021a
#module load R/4.3.2-foss-2023a
## module load Pandoc/2.7.3
#module load Pandoc/3.1.2

source ~/miniconda3/etc/profile.d/conda.sh
conda activate tbeeprotpip_conda

#export your required environment variables below
#################################################
#COMPARISONS=$1
COMPARISON_NAME=$(cat $1 | head -n $SLURM_ARRAY_TASK_ID | tail -n 1 | awk {'print $1'})
OUTPUT_NAME="${COMPARISON_NAME}_output"
ANNOTATION_FILE="annotation_file_${COMPARISON_NAME}.csv"

#MAIN_PATH=$1
#COMPARISON_NAME=$2
#EXPERIMENT_NAME=$3
#ANNOTATION_FILE=$4

#Rscript tbeeprotpip_runfile.R -m /scicore/home/gagneux/bousel00/tb/TbX015 -f Mtb_Proteomics_Pipeline_worksheet.Rmd -o WT_vs_S450L_output -i /scicore/home/gagneux/bousel00/tb/TbX015/spectronaut_report_files/20230804_153908_P462_SB_TbX015_directDIA_Report.tsv -x TbX015 -a annotation_file_wt_vs_s450l.csv
#Rscript /scicore/home/gagneux/GROUP/proteomics/tbeeprotpip_Rpackage/inst/pipeline_runfile/tbeeprotpip_runfile.R -m $MAIN_PATH -f Mtb_Proteomics_Pipeline_worksheet.Rmd -o $COMPARISON_NAME -i /scicore/home/gagneux/GROUP/proteomics/TbX016/spectronaut_report_files/20241021_082708_P462_SB_sb009_directDIA_Report_forProtti.tsv -x $EXPERIMENT_NAME -a $ANNOTATION_FILE
Rscript /scicore/home/gagneux/GROUP/proteomics/tbeeprotpip_Rpackage/inst/pipeline_runfile/tbeeprotpip_runfile.R -m /scicore/home/gagneux/GROUP/proteomics/TbX016 -f Mtb_Proteomics_Pipeline_worksheet.Rmd -o $OUTPUT_NAME -i /scicore/home/gagneux/GROUP/proteomics/TbX016/spectronaut_report_files/20241021_082708_P462_SB_sb009_directDIA_Report_forProtti.tsv -x TbX016 -a $ANNOTATION_FILE

conda deactivate

chmod 777 /scicore/home/gagneux/GROUP/proteomics/TbX016/output_files/$COMPARISON_NAME*
