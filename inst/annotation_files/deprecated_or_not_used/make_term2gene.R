#!/scicore/soft/apps/R/4.2.2-foss-2021a/bin/R --vanilla

### Script - Mtb proteome analysis TERM2GENE file generation - Selim Bouaouina
### Mycobacterium tuberculosis

# this new version of the script works with protein annotation generated with the "string-db.org" > Add organism (Version 12.0 as of 17. October 2023),
# where you can upload a proteome fasta (in this case the UniprotProteomes reference proteome of Mtb H37Rv as of 16. October 2023) and it will annotate all proteins with it's algorythm.
# For me this turned out to be the better solution, as the QuickGO annotation turned out not to be complete. Example of why it was not complete:
# Rv3404c, is uniprot ID P9WKZ3. In QuickGO only listed under "translational initiation GO:0006413" not "translation GO:0006412"
# Rv1640c, is uniprot ID P9WFU7. In QuickGO only listed under "tRNA aminoacylation for protein translation [GO:0006418]" not "translation GO:0006412"
# The problem seems to be the lacking parent terms in my QuickGo Database
# --> so when running GSEA, "translation" could not even be enriched, because not all necessary terms carry the GO term.


# first define the function to load the latest file. It's in the pipeline, but not sure if this code has access to it.
latest_file = function(main_path, prefix){
  all_respective_files_in_dir = dir(paste(main_path,"annotation_files", sep="/"))[grepl(paste("fetch_", prefix ,"_output.Rdata", sep=""),dir(paste(main_path,"annotation_files", sep="/")))]
  all_respective_files_in_dir_sorted = sort(all_respective_files_in_dir, decreasing=TRUE)
  paste(main_path, "annotation_files", all_respective_files_in_dir_sorted[1], sep="/")
}

# define custom function to shorten strings
shorten_string <- function(input_str, max_length = 60) {
  if (nchar(input_str) <= max_length) {
    return(input_str)
  } else {
    # Calculate the length of the prefix and suffix
    prefix_length <- floor((max_length - 5) / 2)
    suffix_length <- max_length - 5 - prefix_length

    # Create the shortened string with "[...]"
    shortened_str <- paste0(substr(input_str, 1, prefix_length),
                            " [...]",
                            substr(input_str, nchar(input_str) - suffix_length + 1, nchar(input_str)))

    return(shortened_str)
  }
}



# define the function making the file from the uniprot input
make_t2g = function(input_file, kingdom){
  if(kingdom=="CC"){data=data.frame(input_file[input_file$category =="Cellular_Component_(Gene_Ontology)",])} # "Cellular_Component_(Gene_Ontology)"
  if(kingdom=="BP"){data=data.frame(input_file[input_file$category =="Biological_Process_(Gene_Ontology)",])} # "Biological_Process_(Gene_Ontology)"
  if(kingdom=="MF"){data=data.frame(input_file[input_file$category =="Molecular_Function_(Gene_Ontology)",])} # "Molecular_Function_(Gene_Ontology)"
  if(kingdom=="AK"){data=data.frame(input_file[input_file$category =="Annotated_Keywords_(UniProt)",])} # "Annotated_Keywords_(UniProt)"
  if(kingdom=="LNC"){data=data.frame(input_file[input_file$category =="Local_Network_Cluster_(STRING)",])} # "Local_Network_Cluster_(STRING)"
  if(kingdom=="KEGG"){data=data.frame(input_file[input_file$category =="KEGG_(Kyoto_Encyclopedia_of_Genes_and_Genomes)",])} # "KEGG_(Kyoto_Encyclopedia_of_Genes_and_Genomes)"
  if(kingdom=="SCL"){data=data.frame(input_file[input_file$category =="Subcellular_localization_(COMPARTMENTS)",])} # "Subcellular_localization_(COMPARTMENTS)"
  if(kingdom=="RP"){data=data.frame(input_file[input_file$category =="Reactome_Pathways",])} # "Reactome_Pathways"
  if(!(kingdom %in% c("CC","BP","MF", "AK", "LNC", "KEGG", "SCL", "RP"))){print("please choose one of the kingdoms 'CC', 'BP', 'MF', 'AK', 'LNC', 'KEGG', 'SCL', 'RP'")}
  output_table = unique(data[,c("temp_label","pg_protein_accession")])
  names(output_table)[1] = "term"
  return(output_table)
}


# load the stringdb annotation file
#sdb_data = read.delim("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/stringdb_STRG0A14UTP_protenrichterms_v12.0_231016.txt", header=T, sep="\t")
sdb_data = read.delim(paste(system.file("annotation_files",  package="tbeeprotpip"),"stringdb_STRG0A14UTP_protenrichterms_v12.0_231016.txt", sep="/"), header=T, sep="\t")
sdb_data$category = gsub(" ","_", sdb_data$category)
names(sdb_data)[1] = "pg_protein_accession"
sdb_data$pg_protein_accession = gsub("STRG0A14UTP.","", sdb_data$pg_protein_accession)
sdb_data$temp_label = paste(sdb_data$description, "-[", sdb_data$term, "]", sep="")
# apply the shorten_string function to the temp_label column
sdb_data$temp_label = sapply(sdb_data$temp_label, shorten_string)




# generate the files based on only the sdb_data input
CC_TERM2GENE = make_t2g(sdb_data, "CC")
BP_TERM2GENE = make_t2g(sdb_data, "BP")
MF_TERM2GENE = make_t2g(sdb_data, "MF")
AK_TERM2GENE = make_t2g(sdb_data, "AK")
LNC_TERM2GENE = make_t2g(sdb_data, "LNC")
KEGG_TERM2GENE = make_t2g(sdb_data, "KEGG")
SCL_TERM2GENE = make_t2g(sdb_data, "SCL")
RP_TERM2GENE = make_t2g(sdb_data, "RP")


# write the files based only on uniprot data only
write.table(CC_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_CC_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(BP_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_BP_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(MF_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_MF_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(AK_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_AK_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(LNC_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_LNC_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(KEGG_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_KEGG_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(SCL_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_SCL_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")
write.table(RP_TERM2GENE, paste("/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/sigdb_RP_TERM2GENE_",format(Sys.time(), "%Y%m%d"),".tsv", sep=""), col.names=T, row.names=F, quote=F, sep="\t")





