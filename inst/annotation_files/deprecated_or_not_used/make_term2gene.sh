#!/bin/bash
#SBATCH --job-name=make_term2gene_files
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=64G              #This is the memory reserved per core.
#Total memory reserved: 128GB

#SBATCH --time=06:00:00
#SBATCH --qos=6hours 

#SBATCH --output=/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/%x.%j.o
#SBATCH --error=/scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/%x.%j.e

module load R/4.2.0-foss-2021a
R --vanilla < /scicore/home/gagneux/bousel00/tb/tbeeprotpip/inst/annotation_files/make_term2gene.R

