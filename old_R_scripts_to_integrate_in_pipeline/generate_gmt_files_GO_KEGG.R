#!/scicore/soft/apps/R/4.2.0-foss-2021a/bin/R --vanilla

### Script - Mtb proteome analysis gmt file generation - Selim Bouaouina
### Mycobacterium tuberculosis


MAIN_PATH = "/scicore/home/gagneux/bousel00/tb/proteomics/MTB_proteome_pipeline"


####### Uniprot annotation (excluding GO terms) | see script fetch_uniprot_separate_script.r
#automatically load the latest version of the annotation file
uniprot_files <- dir(paste(MAIN_PATH,"annotation_files", sep="/"))[grepl("fetch_uniprot_output.Rdata",dir(paste(MAIN_PATH,"annotation_files", sep="/")))]
uniprot_files <- sort(uniprot_files, decreasing=TRUE)
load(paste(MAIN_PATH, "annotation_files", uniprot_files[1], sep="/")) # loads the lastest version of the data set "uniprot"

##############
#######
####### Mycobrowser annotation | downloaded from mycobrowser website on 24. May 2022
mycobrowser_data <- read.csv("annotation_files/mycobrowser_Mycobacterium_tuberculosis_H37Rv_txt_v4.txt", header=T, sep="\t")

##############
#######
####### KEGG annotation
#automatically load the latest version of the annotation file
kegg_files <- dir(paste(MAIN_PATH,"annotation_files", sep="/"))[grepl("fetch_kegg_output.Rdata",dir(paste(MAIN_PATH,"annotation_files", sep="/")))]
kegg_files <- sort(kegg_files, decreasing=TRUE)
load(paste(MAIN_PATH, "annotation_files", kegg_files[1], sep="/")) # loads the lastest version of the data set "kegg_list"


## GO_CC GSEA
# generate .gmt file from the loaded "uniprot" table
go_cc <- data.frame(uniprot[c("pg_protein_accessions","go_cellular_component")])

# make vector of all occurring go_cc_terms
go_cc_terms <- go_cc$go_cellular_component
go_cc_terms <- go_cc_terms[!is.na(go_cc_terms)]
go_cc_terms <- paste(go_cc_terms, collapse="; ")
go_cc_terms <- unique(unlist(strsplit(go_cc_terms, split = "; ")))
go_cc_terms <- go_cc_terms[!is.na(go_cc_terms)]

# write a loop, which checks if the go_cc_term appears for a protein
cc_table_for_gmt <- data.frame(matrix(ncol = 2, nrow = 0))
for (row in c(1:nrow(go_cc))){
  for (go_term in go_cc_terms){
    ifelse(grepl(go_term, go_cc[row,2], fixed=TRUE), 
    cc_table_for_gmt <- rbind(cc_table_for_gmt, c(go_term, go_cc[row,1])),NA)
  }
}
colnames(cc_table_for_gmt) <- c("go_term", "pg_protein_accessions")

# summarize the table, so all proteins occurring in one go_term, appear on the same line
#   go_cc_gmt <- aggregate(pg_protein_accessions ~ go_term, data = table_for_gmt, paste, collapse = ",")
#   stringr::str_split_fixed(go_cc_gmt$pg_protein_accessions, ",",n=(Inf))
#   go_cc_gmt <- cbind(go_cc_gmt,stringr::str_split_fixed(go_cc_gmt$pg_protein_accessions, ",",n=(Inf)))
#   colnames(go_cc_gmt) <- NULL

# found out, that gmt file for fgsea must be a list anyway. So simply split loop output
go_cc_gmt <- split(cc_table_for_gmt$pg_protein_accessions, cc_table_for_gmt$go_term)

# generate .gmt file from the loaded "uniprot" table
go_bp <- data.frame(uniprot[c("pg_protein_accessions","go_biological_process")])

# make vector of all occurring go_bp_terms
go_bp_terms <- go_bp$go_biological_process
go_bp_terms <- go_bp_terms[!is.na(go_bp_terms)]
go_bp_terms <- paste(go_bp_terms, collapse="; ")
go_bp_terms <- unique(unlist(strsplit(go_bp_terms, split = "; ")))
go_bp_terms <- go_bp_terms[!is.na(go_bp_terms)]

# write a loop, which checks if the go_bp_term appears for a protein
bp_table_for_gmt <- data.frame(matrix(ncol = 2, nrow = 0))
for (row in c(1:nrow(go_bp))){
  for (go_term in go_bp_terms){
    ifelse(grepl(go_term, go_bp[row,2], fixed=TRUE), 
    bp_table_for_gmt <- rbind(bp_table_for_gmt, c(go_term, go_bp[row,1])),NA)
  }
}
colnames(bp_table_for_gmt) <- c("go_term", "pg_protein_accessions")

# summarize the table, so all proteins occurring in one go_term, appear on the same line
#   go_bp_gmt <- aggregate(pg_protein_accessions ~ go_term, data = table_for_gmt, paste, collapse = ",")
#   stringr::str_split_fixed(go_bp_gmt$pg_protein_accessions, ",",n=(Inf))
#   go_bp_gmt <- cbind(go_bp_gmt,stringr::str_split_fixed(go_bp_gmt$pg_protein_accessions, ",",n=(Inf)))
#   colnames(go_bp_gmt) <- NULL

# found out, that gmt file for fgsea must be a list anyway. So simply split loop output
go_bp_gmt <- split(bp_table_for_gmt$pg_protein_accessions, bp_table_for_gmt$go_term)

# generate .gmt file from the loaded "uniprot" table
go_mf <- data.frame(uniprot[c("pg_protein_accessions","go_molecular_function")])

# make vector of all occurring go_mf_terms
go_mf_terms <- go_mf$go_molecular_function
go_mf_terms <- go_mf_terms[!is.na(go_mf_terms)]
go_mf_terms <- paste(go_mf_terms, collapse="; ")
go_mf_terms <- unique(unlist(strsplit(go_mf_terms, split = "; ")))
go_mf_terms <- go_mf_terms[!is.na(go_mf_terms)]

# write a loop, which checks if the go_mf_term appears for a protein
mf_table_for_gmt <- data.frame(matrix(ncol = 2, nrow = 0))
for (row in c(1:nrow(go_mf))){
  for (go_term in go_mf_terms){
    ifelse(grepl(go_term, go_mf[row,2], fixed=TRUE), 
    mf_table_for_gmt <- rbind(mf_table_for_gmt, c(go_term, go_mf[row,1])),NA)
  }
}
colnames(mf_table_for_gmt) <- c("go_term", "pg_protein_accessions")

# summarize the table, so all proteins occurring in one go_term, appear on the same line
#   go_mf_gmt <- aggregate(pg_protein_accessions ~ go_term, data = table_for_gmt, paste, collapse = ",")
#   stringr::str_split_fixed(go_mf_gmt$pg_protein_accessions, ",",n=(Inf))
#   go_mf_gmt <- cbind(go_mf_gmt,stringr::str_split_fixed(go_mf_gmt$pg_protein_accessions, ",",n=(Inf)))
#   colnames(go_mf_gmt) <- NULL

# found out, that gmt file for fgsea must be a list anyway. So simply split loop output
go_mf_gmt <- split(mf_table_for_gmt$pg_protein_accessions, mf_table_for_gmt$go_term)



save(go_cc_gmt, file = paste(MAIN_PATH,"annotation_files","go_cc_gmt.Rdata", sep="/"))
save(cc_table_for_gmt, file = paste(MAIN_PATH,"annotation_files","cc_table_for_gmt.Rdata", sep="/"))
save(go_bp_gmt, file = paste(MAIN_PATH,"annotation_files","go_bp_gmt.Rdata", sep="/"))
save(bp_table_for_gmt, file = paste(MAIN_PATH,"annotation_files","bp_table_for_gmt.Rdata", sep="/"))
save(go_mf_gmt, file = paste(MAIN_PATH,"annotation_files","go_mf_gmt.Rdata", sep="/"))
save(mf_table_for_gmt, file = paste(MAIN_PATH,"annotation_files","mf_table_for_gmt.Rdata", sep="/"))

##############
#######

# no gmt file needed, as I'm not applying the fgsea function here. need an adapted TERM2GENE table though
# write a loop, which reshuffles the kegg_list, so it's useful for GSEA input

kegg_orgids <- names(kegg_list)
GSEA_kegg_list <- list()
for (org_id in kegg_orgids){
  tmp_subset <- kegg_list[[org_id]]
  tmp_subset$label_joined <- paste(tmp_subset$pathway_id, tmp_subset$pathway_name, sep="_")
  GSEA_kegg_list[[org_id]] <- tmp_subset[,c("label_joined","uniprot_id")]
}  

save(GSEA_kegg_list, file = paste(MAIN_PATH,"annotation_files","GSEA_kegg_list.Rdata", sep="/"))




